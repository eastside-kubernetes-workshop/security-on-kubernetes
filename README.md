# Resources

The Eastside Kubernetes Workshop host's take on security:
https://www.tcbtech.com/secure-k8s
(Built for the June 2020 joint Workshop with the [Seattle CSA, the Cloud Security Alliance](https://chapters.cloudsecurityalliance.org/seattle/))
